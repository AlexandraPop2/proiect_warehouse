import presentation.Controller;
import presentation.View;

/**
 * @author Pop Alexandra
 * Clasa main ce contine un obiect de tip View si unul de tip Controller si face UI vizibila
 */
public class Main {
    public static void main(String args[]){
        View view = new View();
        Controller controller = new Controller(view);
        view.setVisible(true);
    }
}
