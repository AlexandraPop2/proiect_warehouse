package model;

/**
 * @author  Pop Alexandra
 * Clasa care se mapeaza pe tabela client din baza de date, contine toate atributele acesteia si contine getters si
 * setters pentru fiecare in parte.
 */

public class Client {
    private Integer id;
    private String name;
    private String address;
    private String email;
    private Integer age;

    public Client(Integer id, Integer age, String name, String add, String email){
        this.id = id;
        this.name = name;
        this.address = add;
        this.email = email;
        this.age = age;
    }

    public Client(){};

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer idClient) {
        this.id = idClient;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "Client with id = "+this.id+", name "+this.name+", address "+this.address+", email "+this.email+", age "+this.age;
    }
}
