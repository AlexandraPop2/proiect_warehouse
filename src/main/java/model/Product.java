package model;

/**
 * @author Pop Alexandra
 * Clasa Producr se mapeaza pe tabela product din baza de date la care suntem conectati si contine getters si setters pentru
 * fiecare atribut.
 */

public class Product {
    private Integer id;
    private String name;
    private Integer stock;
    private Integer price;

    public Product(Integer id, String name, Integer stk, Integer price){
        this.id = id;
        this.name = name;
        this.stock = stk;
        this.price = price;
    }

    public Product(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer idProduct) {
        this.id = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String toString(){
        return "Product with id = "+this.id+", name "+this.name+", stock "+this.stock+", price "+this.price;
    }
}
