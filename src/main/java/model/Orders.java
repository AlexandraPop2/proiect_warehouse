package model;

/**
 * @author  Pop Alexandra
 * Clasa Orders se mapeaza pe tabela order din baza de date tema3 si contine getters si setters pentru fiecare atribut.
 */
public class Orders {
    private Integer id;
    private Integer idClient;
    private Integer idProduct;
    private Integer quantity;

    public Orders(Integer idO, Integer idC, Integer idP, Integer q){
        this.id = idO;
        this.idClient = idC;
        this.idProduct = idP;
        this.quantity = q;
    }

    public Orders(){

    }

    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer idOrder) {
        this.id = idOrder;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String toString(){
        return "\nProduct id: "+this.idProduct+"\nClient id: "+this.idClient+"\nQuantity: "+this.quantity;
    }
}
