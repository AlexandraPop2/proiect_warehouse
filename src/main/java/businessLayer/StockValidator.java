package businessLayer;

import model.Product;

/**
 * @author Pop Alexandra
 * Este o clasa ce verifica daca produsul ce se cumpara are o cantitate suficienta in stoc in functie de cat se doreste sa
 * se cumpere.
 */

public class StockValidator {

    public static boolean validateStock(Product p, Integer quantity){
        if(p.getStock() - quantity < 0){
            return false;
        }
        else{
            return true;
        }
    }

}
