package businessLayer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Pop Alexandra
 * Clasa care valideaza email-urile cu care se insereaza un client in tabela client.
 */

public class EmailValidator {
    private static final String EMAIL = "[a-z]*[A-Z]*[a-z]+[0-9]*@[a-z]+\\.com";

    public static boolean validateEmail(String email){
        Pattern pattern = Pattern.compile(EMAIL);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
