package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Vector;

/**
 * @author Pop Alexandra
 * Clasa ce contine interfata grafica. Are un panel pentru fereastra de pornire ce contine butoanele catre celelalte 3 ferestre.
 * Contine si alte 4 panel-uri, cate unul pentru lucrul cu client-ul, produsul, comanda si unul pentru afisarea tuturor tuplelor
 * dintr-un table cu JTable.
 * Contine, de asemenea, si metode accesorare si setatoare, si action listeners pentru fiecare buton.
 */
public class View extends JFrame {
    private JPanel mainPanel = new JPanel();
    private JPanel clientPanel = new JPanel();
    private JPanel productPanel = new JPanel();
    private JPanel orderPanel = new JPanel();
    private JPanel viewPanel = new JPanel();
    private JLabel lbl = new JLabel("Alegeti pagina dorita:");
    private JButton btnClient = new JButton("Client Page");
    private JButton btnProduct = new JButton("Product Page");
    private JButton btnOrders = new JButton("Order Page");

    private JTextField id = new JTextField("id");
    private JTextField name = new JTextField("name");
    private JTextField address = new JTextField("address");
    private JTextField email = new JTextField("email");
    private JTextField age = new JTextField("age");

    private JButton addBtn = new JButton("ADD");
    private JButton editBtn = new JButton("EDIT");
    private JButton deleteBtn = new JButton("DELETE");
    private JButton viewBtn = new JButton("VIEW");
    private JButton backBtn1 = new JButton("BACK");

    private JTextField idP = new JTextField("id");
    private JTextField nameP = new JTextField("name");
    private JTextField stock = new JTextField("stock");
    private JTextField price = new JTextField("price");
    private JButton backBtn2 = new JButton("BACK");

    private JButton addPBtn = new JButton("ADD");
    private JButton editPBtn = new JButton("EDIT");
    private JButton deletePBtn = new JButton("DELETE");
    private JButton viewPBtn = new JButton("VIEW");

    private JTextField idC = new JTextField("id Client");
    private JTextField idPr = new JTextField("id Product");
    private JTextField quantity = new JTextField("quantity");
    private JButton backBtn3 = new JButton("BACK");

    private JButton addOBtn = new JButton("ADD ORDER");
    private JButton viewOrders = new JButton("VIEW ORDERS");

    private JButton backBtn4 = new JButton("BACK");

    public View(){

        addToClientPanel();
        addToProductPanel();
        addToOrderPanel();

        viewPanel.setPreferredSize(new Dimension(500, 500));

        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.setPreferredSize(new Dimension(500, 500));
        mainPanel.add(Box.createRigidArea(new Dimension(150, 150)));
        mainPanel.add(lbl);
        mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        mainPanel.add(btnClient);
        mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        mainPanel.add(btnProduct);
        mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        mainPanel.add(btnOrders);

        this.setContentPane(mainPanel);
        this.pack();
        this.setTitle("Database Program");
        this.setPreferredSize(new Dimension(500, 500));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void addToClientPanel(){
        clientPanel.setLayout(new BoxLayout(clientPanel, BoxLayout.Y_AXIS));
        clientPanel.setPreferredSize(new Dimension(500, 500));
        clientPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        clientPanel.add(id);
        clientPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        clientPanel.add(name);
        clientPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        clientPanel.add(address);
        clientPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        clientPanel.add(email);
        clientPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        clientPanel.add(age);
        clientPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        clientPanel.add(addBtn);
        clientPanel.add(editBtn);
        clientPanel.add(deleteBtn);
        clientPanel.add(viewBtn);
        clientPanel.add(backBtn1);
    }

    public void addToProductPanel(){
        productPanel.setLayout(new BoxLayout(productPanel, BoxLayout.Y_AXIS));
        productPanel.setPreferredSize(new Dimension(500, 500));
        clientPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        productPanel.add(idP);
        productPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        productPanel.add(nameP);
        productPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        productPanel.add(stock);
        productPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        productPanel.add(price);
        productPanel.add(Box.createRigidArea(new Dimension(125, 10)));
        productPanel.add(addPBtn);
        productPanel.add(editPBtn);
        productPanel.add(deletePBtn);
        productPanel.add(viewPBtn);
        productPanel.add(backBtn2);
    }

    public void addToOrderPanel(){
        orderPanel.setLayout(new BoxLayout(orderPanel, BoxLayout.Y_AXIS));
        orderPanel.setPreferredSize(new Dimension(500, 500));
        orderPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        orderPanel.add(idC);
        orderPanel.add(Box.createRigidArea(new Dimension(0, 50)));
        orderPanel.add(idPr);
        orderPanel.add(Box.createRigidArea(new Dimension(0, 50)));
        orderPanel.add(quantity);
        orderPanel.add(Box.createRigidArea(new Dimension(125, 50)));
        orderPanel.add(addOBtn);
        orderPanel.add(viewOrders);
        orderPanel.add(backBtn3);
    }

    /**
     * Metoda ce schimba panoul afisat de catre UI, in functie de ce fereastra doreste utilizatorul sa vada si de ce buton apasa
     * @param i
     */
    public void changePanel(int i){
        if(i == 0){
            this.setContentPane(clientPanel);
            this.revalidate();
            this.repaint();
        }
        else if(i == 1){
            this.setContentPane(productPanel);
            this.revalidate();
            this.repaint();
        }
        else if(i == 2){
            this.setContentPane(orderPanel);
            this.revalidate();
            this.repaint();
        }
        else if(i == 3){
            this.setContentPane(mainPanel);
            this.revalidate();
            this.repaint();
        }
    }

    /**
     * Metoda care afiseaza intr-un JTable toate valorile din vectorii dati ca parametri de intrare
     * @param data
     * @param column
     */
    public void viewInformations(Vector data, Vector column){
        viewPanel.removeAll();
        JTable jt = new JTable(data, column);
        JScrollPane sp = new JScrollPane(jt);
        viewPanel.add(sp);
        viewPanel.add(backBtn4);
        this.setContentPane(viewPanel);
        this.revalidate();
        this.repaint();
    }

    public void addBtnClientListener(ActionListener e){
        btnClient.addActionListener(e);
    }

    public void addBtnProductListener(ActionListener e){
        btnProduct.addActionListener(e);
    }

    public void addBtnOrdersListener(ActionListener e){
        btnOrders.addActionListener(e);
    }

    public void addBackBtnListener(ActionListener e){
        backBtn1.addActionListener(e);
        backBtn2.addActionListener(e);
        backBtn3.addActionListener(e);
        backBtn4.addActionListener(e);
    }

    public void addAddCBtnListener(ActionListener e){
        addBtn.addActionListener(e);
    }

    public void addEditCBtnListener(ActionListener e){
        editBtn.addActionListener(e);
    }

    public void addDeleteCBtnListener(ActionListener e){
        deleteBtn.addActionListener(e);
    }

    public void addViewCBtnListener(ActionListener e){
        viewBtn.addActionListener(e);
    }

    public void addAddPBtnListener(ActionListener e){ addPBtn.addActionListener(e); }

    public void addEditPBtnListener(ActionListener e){
        editPBtn.addActionListener(e);
    }

    public void addDeletePBtnListener(ActionListener e){
        deletePBtn.addActionListener(e);
    }

    public void addViewPBtnListener(ActionListener e){
        viewPBtn.addActionListener(e);
    }

    public void addAddOBtnListener(ActionListener e){
        addOBtn.addActionListener(e);
    }

    public void addViewOBtnListener(ActionListener e){ viewOrders.addActionListener(e); }

    //getters:
    public String getIDC(){
        return id.getText();
    }

    public String getNameC(){
        return name.getText();
    }

    public String getAddressC(){
        return address.getText();
    }

    public String getEmailC(){
        return email.getText();
    }

    public String getAgeC(){
        return age.getText();
    }

    public String getIDP(){ return idP.getText(); }

    public String getNameP(){ return nameP.getText(); }

    public String getStockP(){ return stock.getText(); }

    public String getPriceP(){ return price.getText(); }

    public String getIdC(){ return idC.getText(); }

    public String getIdPr(){ return idPr.getText(); }

    public String getQuantity(){ return quantity.getText(); }

}