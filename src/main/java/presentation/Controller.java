package presentation;

import businessLayer.EmailValidator;
import businessLayer.StockValidator;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.OrdersDAO;
import dataAccessLayer.ProductDAO;
import model.Client;
import model.Orders;
import model.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Vector;


/**
 * @author Pop Alexandra
 * Clasa contine o variabila de tipul interfata grafica si contine clase interne pentru fiecare implementare a unui
 * action listener, apelandu-se, in functie de operatie, metode din AbstractDAO pentru lucrul cu baza de date.
 */
public class Controller {
    private View view;

    public Controller(View view){
        this.view = view;
        view.addBtnClientListener(new BtnClientListener()); view.addBtnProductListener(new BtnProductListener());
        view.addBtnOrdersListener(new BtnOrdersListener()); view.addBackBtnListener(new BackBtnListener());
        view.addAddCBtnListener(new AddClientBtnListener()); view.addEditCBtnListener(new EditClientBtnListener());
        view.addDeleteCBtnListener(new DeleteClientBtnListener()); view.addViewCBtnListener(new ViewClientBtnListener());
        view.addAddPBtnListener(new AddProductBtnListener()); view.addEditPBtnListener(new EditProductBtnListener());
        view.addDeletePBtnListener(new DeleteProductBtnListener()); view.addViewPBtnListener(new ViewProductBtnListener());
        view.addAddOBtnListener(new AddOrderBtnListener()); view.addViewOBtnListener(new ViewOrderBtnListener());
    }

    /**
     * Clasa ce implementeaza action listener pentru butonul de schimbare fereastra client
     */
    class BtnClientListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            view.changePanel(0);
        }
    }

    /**
     * Clasa ce implementeaza action listener pentru butonul de schimbare fereastra produs
     */
    class BtnProductListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            view.changePanel(1);
        }
    }

    /**
     * Clasa ce implementeaza action listener pentru butonul de schimbare fereastra produs
     */
    class BtnOrdersListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            view.changePanel(2);
        }
    }

    /**
     * Clasa ce implementeaza action listener pentru butoanele de back.
     */
    class BackBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            view.changePanel(3);
        }
    }

    /**
     * Clasa ce implementeaza functionalitatea pentru butonul de adaugare a unui client in tabela client
     */
    class AddClientBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String name = view.getNameC(); String address = view.getAddressC(); String email = view.getEmailC();
            Integer age = null;
            try{ age = Integer.parseInt(view.getAgeC()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(view, "Age-ul nu este introdus corect!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE); }

            ClientDAO cDao = new ClientDAO(); Client client = new Client();
            client.setName(name); client.setAddress(address);
            client.setEmail(email); client.setAge(age);

            if(EmailValidator.validateEmail(client.getEmail()) == false){
                JOptionPane.showMessageDialog(view, "Email invalid!", "Mesaj succes", JOptionPane.INFORMATION_MESSAGE);

            }
            else if(age != null && cDao.createRow(client) != -1){
                JOptionPane.showMessageDialog(view, "Client creat cu succes!", "Mesaj succes", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(view, "Esuare la creare!", "Mesaj esec", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    /**
     * Clasa ce implementeaza functionalitatea pentru butonul de editare a unui client in tabela client
     */
    class EditClientBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Integer id = null;
            try{ id = Integer.parseInt(view.getIDC()); }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(view, "Id-ul nu este introdus corect!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE);
            }

            String name = view.getNameC(); String address = view.getAddressC();
            String email = view.getEmailC(); Integer age = null; boolean ok = true;
            try{ age = Integer.parseInt(view.getAgeC()); }
            catch(NumberFormatException ex){ ok = false; }

            ClientDAO cDao = new ClientDAO(); Client client = new Client(); client.setId(id);
            if(!name.equals("name")){ client.setName(name); } if(!address.equals("address")){ client.setAddress(address); }
            if(!email.equals("email")){ client.setEmail(email); } if(ok == true){ client.setAge(age); System.out.println();}

            if(EmailValidator.validateEmail(client.getEmail()) == false){
                JOptionPane.showMessageDialog(view, "Email invalid!", "Mesaj succes", JOptionPane.INFORMATION_MESSAGE);
            }
            else if(id != null && cDao.updateRow(client) != -1){
                JOptionPane.showMessageDialog(view, "Client editat cu succes!", "Mesaj succes", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(view, "Esuare la editare!", "Mesaj esec", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    /**
     * Clasa ce implementeaza functionalitatea pentru butonul de stergere a unui client in tabela client
     */
    class DeleteClientBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Integer id = null;
            try{ id = Integer.parseInt(view.getIDC()); }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(view, "Id-ul nu este introdus corect!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE);
            }

            ClientDAO cDao = new ClientDAO();
            if(id != null && cDao.deleteRow(id) != -1){
                JOptionPane.showMessageDialog(view, "Client sters cu succes!", "Mesaj succes", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(view, "Esuare la stergere!", "Mesaj esec", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    /**
     * Clasa ce implementeaza functionalitatea pentru butonul de vizualizare a tuturor clientilor din tabela client
     */
    class ViewClientBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            ClientDAO cDao = new ClientDAO(); List<Client> l = cDao.findAll(); Vector data = new Vector();
            for(Client ct: l){
                Vector v = new Vector();
                v.add(ct.getId()); v.add(ct.getName()); v.add(ct.getAddress()); v.add(ct.getEmail()); v.add(ct.getAge());
                data.add(v);
            }
            Vector column = new Vector();
            column.add("id"); column.add("name"); column.add("address"); column.add("email"); column.add("age");
            view.viewInformations(data, column);
        }
    }

    /**
     * Clasa ce implementeaza functionalitatea pentru butonul de adaugare a unui produs in tabela product
     */
    class AddProductBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String name = view.getNameP(); Integer stock = null, price = null;
            try{ stock = Integer.parseInt(view.getStockP()); price = Integer.parseInt(view.getPriceP()); }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(view, "Stock-ul sau Price-ul nu este introdus corect!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE);
            }

            ProductDAO pDao = new ProductDAO(); Product p = new Product();
            p.setName(name); p.setPrice(price); p.setStock(stock);
            if(price != null && stock != null && pDao.createRow(p) != -1){
                JOptionPane.showMessageDialog(view, "Produs creat cu succes!", "Mesaj succes", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(view, "Esuare la creare!", "Mesaj esec", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    /**
     * Clasa ce implementeaza functionalitatea pentru butonul de editare a unui produs in tabela product
     */
    class EditProductBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Integer id = null; Integer stock = null; Integer price = null;
            try{ id = Integer.parseInt(view.getIDP()); }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(view, "Id-ul nu este introdus corect!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE);
            }

            try{ stock = Integer.parseInt(view.getStockP());price = Integer.parseInt(view.getPriceP()); }
            catch(NumberFormatException ex){}
            String name = view.getNameP();

            ProductDAO pDao = new ProductDAO(); Product p = new Product();
            p.setId(id); p.setStock(stock); p.setPrice(price);
            if(!name.equals("name")){ p.setName(name); }

            if(id != null && pDao.updateRow(p) != -1){
                JOptionPane.showMessageDialog(view, "Produs editat cu succes!", "Mesaj succes", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(view, "Esuare la editare!", "Mesaj esec", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    /**
     * Clasa ce implementeaza functionalitatea pentru butonul de stergere a unui produs din tabela product
     */
    class DeleteProductBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Integer id = null;
            try{ id = Integer.parseInt(view.getIDP()); }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(view, "Id-ul nu este introdus corect!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE);
            }

            ProductDAO pDao = new ProductDAO();
            if(id != null && pDao.deleteRow(id) != -1){
                JOptionPane.showMessageDialog(view, "Produs sters cu succes!", "Mesaj succes", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(view, "Esuare la stergere!", "Mesaj esec", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    /**
     * Clasa ce implementeaza functionalitatea pentru butonul de vizualizate a tuturor produselor din tabela product
     */
    class ViewProductBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            ProductDAO pDao = new ProductDAO(); List<Product> l = pDao.findAll(); Vector data = new Vector();
            for(Product ct: l){
                Vector v = new Vector();
                v.add(ct.getId()); v.add(ct.getName()); v.add(ct.getStock()); v.add(ct.getPrice());
                data.add(v);
            }
            Vector column = new Vector();
            column.add("id"); column.add("name"); column.add("stock"); column.add("price");
            view.viewInformations(data, column);
        }
    }

    /**
     *
     * @param order comanda careia ii printam chitanta
     * @param ret id-ul comenzii
     */
    public void writeBill(Orders order, int ret){
        FileWriter fW = null;
        try{ fW = new FileWriter("Bill No"+ret+".txt", true); fW.write("Order id: "+ret); fW.write(order.toString()); fW.close(); }
        catch(IOException e){ e.getMessage(); }
    }

    /**
     * Clasa ce implementeaza functionalitatea pentru butonul de adaugare a unei comenzi in tabela orders
     */
    class AddOrderBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Integer idC = null; Integer idPr = null; Integer quantity = null; boolean ok = true;
            try{ idC = Integer.parseInt(view.getIdC()); }
            catch(NumberFormatException ex) { JOptionPane.showMessageDialog(view, "Id-ul clientului nu este introdus corect!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE); }

            try{ idPr = Integer.parseInt(view.getIdPr()); }
            catch(NumberFormatException ex) { JOptionPane.showMessageDialog(view, "Id-ul produsului nu este introdus corect!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE); }

            try{ quantity = Integer.parseInt(view.getQuantity()); }
            catch(NumberFormatException ex) { JOptionPane.showMessageDialog(view, "Cantitatea nu este introdusa corect!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE); }

            OrdersDAO oDao = new OrdersDAO(); Orders  order = new Orders();
            order.setIdClient(idC); order.setIdProduct(idPr); order.setQuantity(quantity);
            ProductDAO pDao = new ProductDAO(); Product p = new Product(); if(idPr != null){ p = pDao.findById(idPr); }
            ClientDAO cDao = new ClientDAO(); Client c = new Client(); if(idC != null){ c = cDao.findById(idC); }

            if(p == null || c == null){ ok = false; JOptionPane.showMessageDialog(view, "Nu exista clientul sau produsul!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE); }

            if(quantity != null && StockValidator.validateStock(p, quantity) == true){ p.setStock(p.getStock() - quantity); pDao.updateRow(p); }
            else{ ok = false; JOptionPane.showMessageDialog(view, "Nu exista suficiente produse in stock!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE); }

            if(ok == true && quantity != null && idC != null && idPr != null){ int ret = oDao.createRow(order); writeBill(order, ret); JOptionPane.showMessageDialog(view, "Comanda finalizata cu succes!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE); }
            else{ JOptionPane.showMessageDialog(view, "Nu s-a realizat comanda!", "Mesaj warning", JOptionPane.INFORMATION_MESSAGE); }
        }
    }

    /**
     * Clasa ce implementeaza functionalitatea pentru butonul de vizualizare a tuturor comenzilor din tabela orders
     */
    class ViewOrderBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            OrdersDAO oDao = new OrdersDAO(); List<Orders> l = oDao.findAll(); Vector data = new Vector();
            for(Orders ct: l){
                Vector v = new Vector();
                v.add(ct.getId()); v.add(ct.getId()); v.add(ct.getIdClient()); v.add(ct.getIdProduct()); v.add(ct.getQuantity());
                data.add(v);
            }
            Vector column = new Vector();
            column.add("id"); column.add("idClient"); column.add("idProduct"); column.add("Quantity");
            view.viewInformations(data, column);
        }
    }
}
