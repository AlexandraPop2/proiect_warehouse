package dataAccessLayer;
import model.Client;

/**
 * @author  Pop Alexandra
 * Clasa goala care extinde clasa generica AbstractDAO cu tipul Client, pentru a lucra cu tuple din tabelul client.
 */
public class ClientDAO extends AbstractDAO<Client> {
}
