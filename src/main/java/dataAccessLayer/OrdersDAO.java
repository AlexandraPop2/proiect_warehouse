package dataAccessLayer;

import model.Orders;

/**
 * @author  Pop Alexandra
 * Clasa goala care extinde clasa generica AbstractDAO cu tipul Orders, pentru a lucra cu tuple din tabelul orders.
 */

public class OrdersDAO extends AbstractDAO<Orders>{
}
