package dataAccessLayer;

import java.sql.*;
import java.util.logging.Logger;

/**
 * @author Pop Alexandra
 * Clasa care realizeaza conexiunea cu baza de date dorita, si foloseste pattern-ul Singleton.
 */

public class DBConnection {
    /**
     * Contine un Driver, URL, user si password pentru conexiunea cu baza de date.
     */
    private static final Logger LOGGER = Logger.getLogger(DBConnection.class.getName());
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost/tema3";
    private static final String USER = "root";
    private static final String PASSWORD = "bangtanboysbts7MX";

    private static final DBConnection singleInstance = new DBConnection();

    private DBConnection() {
        try{
            Class.forName(DRIVER);
        }
        catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @return
     */
    private static Connection createConnection() {
        Connection conn = null;
        try{
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return conn;
    }

    /**
     *
     * @return
     */
    public static Connection getConnection(){
        return createConnection();
    }

    /**
     *
     * @param conn Conexiunea pe care o inchidem
     */
    public static void close(Connection conn){
        try{
            if(conn != null){
                conn.close();
            }
        }
        catch(SQLException e){
            e.printStackTrace();;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param stm Statement-ul pe care il inchidem
     */
    public static void close(Statement stm){
        try{
            if(stm != null){
                stm.close();
            }
        }
        catch(SQLException e){
            e.printStackTrace();;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param rst ResultSet-ul pe care il inchidem
     */
    public static void close(ResultSet rst){
        try{
            if(rst != null){
                rst.close();
            }
        }
        catch(SQLException e){
            e.printStackTrace();;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

}
