package dataAccessLayer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
* @author  Pop Alexandra
 * Clasa generica ce contine tehnici de reflection pentru a crea, edita, sterge si vizualiza obiecte din baza de date,
 * in functie de tabela in care dorim sa facem operatia.
 * Contine cate o metoda separata pentru gasire in functie de id a unei tuple, gasirea tuturor componentelor unui table,
 * crearea unei intrari in tabel, editarea unei tuple din tabel sau stergerea in functie de id.
 * De asemenea, contine si metode pentru a crea String-ul cu comanda in sql.
* */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO(){
        this.type = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Metoda realizeaza interogarea de select in functie de id pentru un parametru de tip int dat ca intrare
     * @param id id-ul cautat in tabela
     * @return
     */
    public T findById(int id){
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rst = null;
        String query = createSelectQuery("id");
        try{
            conn = DBConnection.getConnection();
            stm = conn.prepareStatement(query);
            stm.setInt(1, id);
            rst = stm.executeQuery();

            List<T> l = createObjects(rst);
            if(!l.isEmpty()){
                return l.get(0);
            }
        }
        catch(SQLException e){
            LOGGER.log(Level.WARNING, type.getName()+ "DAO:findById"+e.getMessage());
        }
        finally {
            DBConnection.close(rst);
            DBConnection.close(stm);
            DBConnection.close(conn);
        }
       return null;
    }

    /**
     * Metoda returneaza o lista de obiecte de tipul generic T, ce va contine toate tuplele din tabelul cu acel tip din bd
     * @return
     */
    public List<T> findAll(){
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rst = null;
        String query = createSelectAllQuery();
        try{
            conn = DBConnection.getConnection();
            stm = conn.prepareStatement(query);
            rst = stm.executeQuery();

            List<T> l = createObjects(rst);
            if(!l.isEmpty()){
                return l;
            }
        }
        catch(SQLException e){
            LOGGER.log(Level.WARNING, type.getName()+ "DAO:findAll"+e.getMessage());
        }
        finally {
            DBConnection.close(rst);
            DBConnection.close(stm);
            DBConnection.close(conn);
        }
        return null;
    }

    /**
     * Metoda realizeaza interogarea de select pe un obiect dat ca parametru de intrare
     * @param obj Obiectul ale carui valori sunt atribuite campurilor clasei T
     * @return
     */
    public int createRow(Object obj){
        Connection conn = null; PreparedStatement stm = null;
        ResultSet rst = null; String query = createInsertQuery();
        try{
            conn = DBConnection.getConnection();
            stm = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            for(Field f: type.getDeclaredFields()){
                f.setAccessible(true);
                try{
                    if(!f.getName().equals("id")){
                        stm.setObject(i, f.get(obj)); i++;
                    }
                }
                catch(IllegalArgumentException | IllegalAccessException e){ e.printStackTrace(); }
            }
            stm.executeUpdate(); rst = stm.getGeneratedKeys();
            if(rst.next()){ return rst.getInt(1); }
        }
        catch(SQLException e){
            LOGGER.log(Level.WARNING, type.getName()+ "DAO: createRow"+e.getMessage());
        }
        finally {
            DBConnection.close(rst); DBConnection.close(stm); DBConnection.close(conn);
        }
        return -1;
    }

    /**
     * Metoda realizeaza interogare de update pe un obiect dat ca parametru intrare
     * @param obj Obiectul ale carui valori sunt atribuite campurilor clasei T
     * @return
     */
    public int updateRow(Object obj){
        Connection conn = null; PreparedStatement stm = null; String query = createUpdateQuery(obj);
        try{
            conn = DBConnection.getConnection(); stm = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            for(Field f: type.getDeclaredFields()){
                f.setAccessible(true);
                try{
                    if(f.get(obj) != null && !f.getName().equals("id")) {
                        System.out.println(f.get(obj)); stm.setObject(i, f.get(obj)); i++;
                    }
                }
                catch(IllegalArgumentException | IllegalAccessException e){ e.printStackTrace(); }
            }
            try{
                Field  f = type.getDeclaredFields()[0]; f.setAccessible(true);
                stm.setObject(i, f.get(obj));
            }
            catch(IllegalAccessException | IllegalArgumentException e){ e.printStackTrace(); }

            System.out.println(stm);
            return stm.executeUpdate();
        }
        catch(SQLException e){ LOGGER.log(Level.WARNING, type.getName()+ "DAO: updateRow"+e.getMessage()); }
        finally {
            DBConnection.close(stm); DBConnection.close(conn);
        }
        return -1;
    }

    /**
     * Metoda realizeaza interogarea de delete pe o intrare de tipul int
     * @param id id-ul folosit pentru a sterge o tupla din tabela
     * @return
     */
    public int deleteRow(int id){
        Connection conn = null; PreparedStatement stm = null; String query = createDeleteQuery();
        try{
            conn = DBConnection.getConnection(); stm = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stm.setInt(1, id);
            return stm.executeUpdate();
        }
        catch(SQLException e){
            LOGGER.log(Level.WARNING, type.getName()+ "DAO: deleteRow"+e.getMessage());
        }
        finally {
            DBConnection.close(stm);
            DBConnection.close(conn);
        }
        return -1;
    }

    /**
     * Metoda returneaza o lista de tipul generic T ce va contine obiecte de tipul T, create prin reflection din result set
     * @param rst ResultSet-ul ale carui valori sunt atribuite unor obiecte de tipul T
     * @return
     */
    private List<T> createObjects(ResultSet rst){
        List<T> l = new ArrayList<T>();
        try{
            while(rst.next()){
                T instance = type.newInstance();
                for(Field f: type.getDeclaredFields()){
                    f.setAccessible(true);
                    Object value = rst.getObject(f.getName());
                    //cu propertyDescriptor accesam metodele accesoare si setatoare pentru field-ul f.
                    PropertyDescriptor pD = new PropertyDescriptor(f.getName(), type);
                    //cu method apelam una dintre aceste metode, aici cea de set(write).
                    Method m = pD.getWriteMethod();
                    m.invoke(instance, value);
                }
                l.add(instance);
            }
        }
        catch(InstantiationException | SQLException | IllegalAccessException | IntrospectionException | IllegalArgumentException | InvocationTargetException e){
            e.printStackTrace();
        }
        return l;
    }

    /**
     * Metoda construieste interogarea pentru select in functie de un field
     * @param field campul dupa care se face select
     * @return
     */
    public String createSelectQuery(String field){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append("* ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    /**
     * Meotda construieste interogarea pentru select *
     * @return
     */
    public String createSelectAllQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append("* ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());
        return sb.toString();
    }

    /**
     * Meotda construieste interogarea pentru insert
     * @return
     */
    public String createInsertQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName());
        sb.append("(");
        for(Field f: type.getDeclaredFields()){
            if(!f.getName().equals("id")) {
                sb.append(f.getName());
                sb.append(", ");
            }
        }
        sb.replace(sb.length() - 2, sb.length() - 1, ") ");
        sb.append(" VALUES (");
       for(Field f: type.getDeclaredFields()){
           if(!f.getName().equals("id")) {
               sb.append("?, ");
           }
       }
        sb.replace(sb.length() - 2, sb.length() - 1, ") ");
        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * Metoda construieste interogarea pentru update
     * @param obj Obiectul ale carui valori seteaza valorile de update
     * @return
     */
    public String createUpdateQuery(Object obj){
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET ");
        for(Field f: type.getDeclaredFields()){
            f.setAccessible(true);
            try {
                if (!f.getName().equals("id") && f.get(obj) != null) {
                    sb.append(f.getName());
                    sb.append(" = ?, ");
                }
            }
            catch(IllegalAccessException e){
                e.printStackTrace();
            }
        }
        sb.replace(sb.length() - 2, sb.length() - 1, " ");
        sb.append("WHERE ");
        sb.append(type.getDeclaredFields()[0].getName());
        sb.append(" = ?;");
        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * Metoda construieste interogarea pentru delete
     * @return
     */
    public String createDeleteQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE ");
        sb.append(type.getDeclaredFields()[0].getName());
        sb.append(" = ?;");
        System.out.println(sb.toString());
        return sb.toString();
    }
}
