package dataAccessLayer;

import model.Product;

/**
 * @author  Pop Alexandra
 * Clasa goala care extinde clasa generica AbstractDAO cu tipul Product, pentru a lucra cu tuple din tabelul product.
 */

public class ProductDAO extends AbstractDAO<Product>{

}
